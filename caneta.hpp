#ifndef CANETA_HPP
#define CANETA_HPP

#include <string>
using namespace std;

class Caneta{

  //Atributos
private:
  string cor;  
  string marca;
  float preco;
  
  //Métodos
public:
  Caneta();   //Construtor
  ~Caneta();  //Destrutor

  //Metodos Acessores Get / Set  
  void setCor(string cor);
  string getCor();
  void setMarca(string marca);
  string getMarca();
  void setPreco(float preco);
  float getPreco();
 
 //Outro Método
  void imprimeDados(); 	

};

#endif
