#include <iostream>
#include "caneta.hpp"

int main(int argc, char**argv){

  Caneta caneta1;
  Caneta caneta2;
  
  Caneta * caneta3;
  caneta3 = new Caneta();

  Caneta * caneta4 = new Caneta();

  caneta1.setCor("Azul");
  caneta1.setMarca("Faber Castell");
  caneta1.setPreco(15.0);

  caneta3->setCor("Vermelha");
  caneta3->setMarca("Pilot");
  caneta3->setPreco(12.00);

  cout << "Caneta1: " << endl;
  caneta1.imprimeDados();
  cout << "Caneta2: " << endl;
  caneta2.imprimeDados();
  cout << "Caneta3: " << endl;
  caneta3->imprimeDados();

  delete caneta3;
  delete caneta4;

  return 0;
}

