#include "caneta.hpp"
#include <iostream>

using namespace std;

Caneta::Caneta(){
  std::cout << "Construtor da Caneta" << std::endl;
  cor   = "Branco";
  marca = "Genérica";
  preco = 1.0;
}

Caneta::~Caneta(){
  std::cout << "Destrutor da Caneta" << std::endl;
}

void Caneta::setCor(string cor){
  this->cor=cor;  
}

string Caneta::getCor(){
  return cor;
}

void Caneta::setMarca(string marca){
  this->marca=marca;
}

string Caneta::getMarca(){
  return marca;
}

void Caneta::setPreco(float preco){
  this->preco=preco;
}

float Caneta::getPreco(){
  return preco;
}

void Caneta::imprimeDados(){
  cout << "Cor: " << cor << endl;
  cout << "Marca: " << marca << endl;
  cout << "Preço: " << preco << endl;
}

